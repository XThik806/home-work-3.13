1. Яке призначення методу event.preventDefault() у JavaScript?

Метод event.preventDefault() у JavaScript використовується для скасування стандартної дії, яка належить події, якщо вона скасовується.

2. В чому сенс прийому делегування подій?

Прийом делегування подій в JavaScript дозволяє обробляти події на вищому рівні DOM-дерева, ніж там, де подія спочатку отримана.

3. Які ви знаєте основні події документу та вікна браузера? 

click, contextmenu, mouseover, mouseout, mousedown, mouseup, mousemove, keydown, keyup