/* Теоретичні питання
1. Яке призначення методу event.preventDefault() у JavaScript?
2. В чому сенс прийому делегування подій?
3. Які ви знаєте основні події документу та вікна браузера? 

Практичне завдання:

Реалізувати перемикання вкладок (таби) на чистому Javascript.

Технічні вимоги:

- У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
- Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
- Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.

 Умови:
 - При реалізації обов'язково використовуйте прийом делегування подій (на весь скрипт обробник подій повинен бути один).
*/


const tabs = document.querySelector('.tabs');
const tabsContent = document.querySelectorAll('.tabs-content li')

tabsContent.forEach((el) => {
    el.style.display = 'none';
    tabsContent[0].style.display = 'block';
})

tabs.addEventListener('click', (event) => {
    let activeButton = document.querySelector(".tabs-title.active");
    const target = event.target.closest('.tabs-title');

    activeButton.classList.remove("active");
    target.classList.add('active');
    activeButton = target;

    let tabClicked = activeButton.textContent.toLowerCase();

    tabsContent.forEach((el) => {
        el.style.display = 'none';
        if(el.dataset.belong === tabClicked) el.style.display = 'block'
    })
})